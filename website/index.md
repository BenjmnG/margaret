# Tutoriel





## Frontmatter 

Toute les fiches projets débute par un _frontmatter_, un chapeau introductif


	+++
	draft = false
	date = "2016-06-01"

	type = "projet"
	layout = "plusieurs_carrousels"
	map = true

	title = "Un Titre"
	id = "un_titre"

	lieu = []
	collaboration_avec = []
	commanditaire =  []
	credit = ""
	remerciement = ""
	performeurs = ""

	description_projet = ""
	mot_cle_projet = []
	auteur_du_projet = []


	[[legende]]
		image = ["01"]
		contexte = ["information", "information deux"]
		description = " Une longue decription <br> Avec plusieurs lignes"

	+++

	Suivra le texte de l'article
	
	

	

Ce _frontmatter_ est un regroupement d'information para-texte. 
Des information qui ne font pas pleinement partie du contenu du projet.
Elles ne sont pas moins importante mais seront appelées petit à petit, suivant nos besoins.

Enfin, une seconde façon de décrire le _frontmatter_ est de le décrire comme une poigné d'information organisé et formaté pour être lu par un script. Que diantre me diras-tu. 

Un exemple, dire : 

> "Le projet A fut réalisé le 10 février 2016, quand le B fut réalisé un mois aprés"

Le script aura un peu de mal à comprendre que nous décrivons deux projet, réalisé à un moi d'interval

Toutefois ecrire : 

    [[Projet A]]
        date = "2016-02-10"

    [[Projet B]]
        date = "2016-03-10"

Ici le script, comprendra.

A noter le frontmatter débute et se termine par `+++`





### Paramètre obligatoire
	
	```
	draft = false 							// Le projet est-il un brouillon (true) ou un projet publié (false)
	date = "2016-06-01"						// Suivant le format Année-Mois-Jour
	type = "projet"							// Ne change pas
	layout = "plusieurs_carrousels"			// Tu as le choix entre 'plusieurs_carrousels' et 'un_carrousel'
	map = false								// Si 'plusieurs_carrousels' alors la valeur est false car le projet ne doit pas être présent dans la liste latérale.
	title = "Un Titre"						// Ecrire comme un humain
	id = "un_titre_a_valeur_d_identifiant"	// Ecrire comme un robot. Cette id doit être le même que celui des images
	```
    
A noter le frontmatter débute et se termine par `+++`




### La bonne formulation pour chaque champs


#### String, une valeur lineaire simple
	
	titre = `Un titre`


#### Multiline string, une valeur simple, repartie sur plusieurs lignes

	description = '''
					Une longue decription
					Très longue

					Très très longue
				  '''

Actuellement le mutliline ne fonctionne pas bien, couplé à une syntaxe markdown ( La chose qui te permet de mettre italic, bold et autre ). Privilegions donc une valeur String avec des balises `<br>`


#### Array, un tableau de donnée

Une liste de valeur, defini par des guillemets anglais, séparé par des virgules, englobé dans des crochets

	["01", "02", "03"]
	["Pierre", "Papier", "Ciseaux rouge"]
    [
        "Nadine",
        "Elaey", 
        "Patty"        
    ]


J'ai choisi la forme de chaque valeur en essayant d'anticiper les cas de figure les plus extrème.
Il convient de les respecter, sinon cela casse.


#### Valeurs imbriquées

Des valeurs imbriqués les unes dans les autres façon Poupées russes
On les reconnait à leurs double crochet, suivit à la ligne d'une valeur String, Array ou multiline.
Cette valeur est décalé par une tabulation


	[[legende]]
		date = ""
		image = ["01", "02"]


### Legende

Etablir une légende se fait en introduisant les paramètres suivant dans le _frontmatter_



	[[legende]]
		image = ["01"]
		contexte = ["information", "information deux"]
		description = " Une longue decription <br> Avec plusieurs lignes"
                    
    
Chaque nouvelle légende doit être réintroduite par`[[legende]]`
A noté que le champ image peut être occupé par plusieurs numéro d'image. La légende s'appliquera alors à toutes ces images.



### Indentation (Tabulation)
    
Il convient de respecter l'indentation
Par exemple, si je souhaite introduire un retour à la ligne dans le champ description d'une image légendé:

Mauvais exemple !

	[[legende]]
        description = " Une longue decription 
    Avec plusieurs lignes"

Le mot "Avec" rompt la structure sémantique. Le robot ne comprendra pas la nature linéaire de ce champ ( String )

	[[legende]]
        description = " Une longue decription <br> Avec plusieurs lignes"



### Image

#### Former un titre

Cela se fait suivant la logique suivante, deux à trois informations séparées par un tiret.

`id-01.jpg`

Donc 

`un_titre_a_valeur_d_identifiant-01.jpg`

`un_titre_a_valeur_d_identifiant-02-si_besoin_de_precision.jpg`

Attention les numéro de l'image conditionne leur ordre d'apparition
Attention, les numéros 1 à 9 commence tous par un 0
01, 02, 03, ...



### Projet 

### Le principe d'ID

Chaque projet inclus un ID. C'est ce qui permettra de faire notamment le liens entre la fiche projet et ses images. 


Nous retrouvons donc l'ID dans le nom de la fiche projet : `le_projet.md`
Si le projet est un groupe de projet, je te conseil de nommer le dossier contenant tout les fichier avec cet ID. Dans ce cas, cette pratique n'a qu'une valeur logique non effective.
Les images : `le_projet-01.jpg`

### Faire groupe 

Pour regrouper plusieurs pieces en une seul page contenant plusieurs caroussels,

Il convient de créer un dossier avec comme nom un ID symbolisant l'ensemble des pièces qu'il contient.
Exemple : `la_ballade_de_kaiser_et_qasimov`

A l'interieure de ce groupe, place ou créer le fiches projets suivant la procedures habituel.

Enfin, il faudra créer un fichier `_index.md`, sorte de fiche projet globale.
Celle-ci contient un _frontmatter_ et un texte d'introduction à tout les projets.
C'est la date contenu dans le _frontmatter_ de cet `_index.md` qui sera prise en compte pour definir la place de ce groupe dans la liste lateral.



### Markdown, rappel de bases

#### Heading	

    # H1
    ## H2
    ### H3


#### Bold 	

    **bold text**
    

#### Italic 	

    *italicized text*
    

#### Blockquote 	

    > blockquote
    

#### Ordered List 

    1. First item
    2. Second item
    3. Third item
    

#### Unordered List 	- First item

    - Second item
    - Third item
    

#### Code 	

    `code`
    

#### Horizontal Rule 	

    ---
    
    
#### Link 	

    [title](https://www.example.com)


### Erreur courante

Des erreur de typo'. Par exemple : 
    
    array = [01", "02"]            // Manque un guillemet avant le 01
    array = [                      // Manque des virgule entre les valeurs
        "01"
        "02"
    ]
