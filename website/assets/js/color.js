///////////////
// Color Random  
//////////////
let root = document.documentElement;
var setColor = [ "rouge", "jaune_pastel", "automne", "azur" ];
var randomSet = setColor[Math.floor(Math.random() * setColor.length)];

if (randomSet == "rouge") {
	root.style.setProperty('--bg_color', '#fed1d1' );
	root.style.setProperty('--color', '#333' );

} else if (randomSet == "jaune_pastel") {
	root.style.setProperty('--bg_color', '#fdfed1' );
	root.style.setProperty('--color', '#a58538' ); //#3a3263
} else if (randomSet == "automne") {
	root.style.setProperty('--bg_color', '#fff' );
	root.style.setProperty('--color', '#24a77b' );
} else if (randomSet == "azur") {
	root.style.setProperty('--bg_color', '#f5fcff' );
	root.style.setProperty('--color', '#e32222' );
} 



//var setColor = [ "#fed1d1", "#fdfed1", "#d1ddfe", "#d1fed8", "#e8d1fe" ];
