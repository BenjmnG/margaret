+++
date = "2018-08-01"
draft = false

type = "projet"
layout= "un_carrousel" 
map = true

title = "Stuff & Feelings"
id = "stuff_feelings"

lieu = [""]
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = "Merci à Mathias Pfund, Francesca Masoero, Laila Hida, Jerome Giller, Amine El Gotaibi, Ayoub Mouzaïne, Benjamin Grémillon, Adeline Molle, Mustapha Beyoud, Slimane Arrabbaje, Nezha Benrital"
performeurs =  ""

[[legende]]
	image = ["01"]
	contexte  = [
		"0% 100%, 2018",
		"tubes néons, transformateurs, fiches, capteur de mouvement, environ 20 x 8 x 6cm",
		"Le 18, Marrakech"
		]
	description = "0% 100% sont deux néons branchés directement à une prise de courant. A travers les arguments marketing 0% 100% qui semblent s’opposer, les concepts de pureté et de confiance cherchent à se mélanger. Leur éclairage simultané au rythme de l’entrée des visiteurs reprend le changement d’apparence de l’espace public accompagnant le passage du roi Mohamed VI ainsi que leur aspect bricolé (tout comme une grande partie de l’éclairage public marrakchi que les habitants doivent prendre en charge par manque de service public) tendent à invalider leur mission."


[[legende]]
	image = ["03"]
	contexte  = [
		"Splendide, 2018",
		"site internet : www.splendide-ma.com/about.html"
		]
	description = "La marque de cosmétiques Splendid dont l’identité se fonde sur la copie de toutes les autres (Nivea, Dove, Veet, Chanel etc) a la partie «à propos» de son site vide. Splendide est donc la copie du site de Splendid dans lequel j’ai rempli le «à propos» d’une fiction nourrie d’images. Cette fiction constitue le fondement narratif de mon projet Stuff & Feelings."

[[legende]]
	image = ["05"]
	contexte  = [
		"à vie, 2018",
		"édition de 250 stickers, 10 x 10 cm",
		"dispersés dans la ville de Marrakech"
		]
	description = "Evaluant la valeur des objets avec humour, les stickers en forme de logo de marques renommées ornent l’électronique et l’automobile marocaines. Avec le V de «à vie», la marque fluide de Splendid se diffuse comme un signe dont on ne connait le sens mais qui propose de nous accompagner éternellement."


[[legende]]
	image = ["06"]
	contexte  = [
		"Gentle Whispering, 2019",
		"affichage dans l’espace public, 180 x 200 cm",
		"Lugano"
		]
	description = "Gentle Whispering est l’une des chaînes youtube d’ASMR les plus suivies. S’endormir ou ressentir du plaisir par la répétition de sons feutrés s’apparente à la prise de contrôle d’un corps muni de son consentement. La fragmentation est aussi la tactique des vendeurs de rue pour gagner un peu de profit. Ce fragment provocateur d’un membre de l’armée royale marocaine a été collé dans l’espace public de la ville de Lugano, au Tessin. Cette région Suisse est connue pour les avantages fiscaux qu’elle accorde aux entreprises dont Gucci qui y a son siège logistique. Rapprocher les tactiques des transporteurs de colis entre le Maroc et la Belgique de celles de Gucci pour tout deux, passer à côté des taxes permet d’interroger l’image, le rôle et le fonctionnement des régulations étatiques."


+++

Stuff & Feelings is a research project that came together through a two-months residency at LE 18, Marrakech.

Departing from a wider interest in informal practices, the project’s focal entry was an attempt to trace and understand the economical, historical, social and emotional relationships moving, and moved by the specific practice of "transport et messagerie", which dispatches packagings between Belgium (more generally European countries with a Moroccan diaspora) and Morocco. Analysing the series of tactics that allow to create space to navigate in between structures, the research delved into the local counterfeit of products as a relevant locus, as the latter performs influence, movement of ideas and shapes, balance of power, while questioning the value of things and destabilizing some common truths.

Creator Alessandro Michele, fashion signature for the Italian brand Gucci, whose labour force is primarily based in Italy (tax rate elevated at 25% of the gross product) though registered in Switzerland (tax rate at 8%), plays with copied topos, such as making overtly visible the logo or mistaking the spelling of Gucci itself by writing Guccy instead.

Twist of fate, François Pinault, former head of Kering, designated the historic customs la Punta della Dogana to host his contemporary art collection. This peculiar v-shaped building looks similar to the logo «à vie» of the Algerian brand Splendid. Dedicated to cosmetics, they created a fluid identity made on copying Chanel, Yves Saint-Laurent, Pierre Cardin, l’Oréal, Veet, Nivea, Axe...

In this framework, the metaphor of the parasite in conflict and sometimes symbiosis with the organism it evolves in, leads the intervention. A series of pieces are scattered, absent and present ; in and out the art space in various temporalities. <br> [Link to website](https://www.splendide-ma.com/about.html) <br><br> The residency program was supported by the Flanders Government - department CJM (Culture, Youth and Media).
