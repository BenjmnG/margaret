+++
draft = false
date = "2014-07-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "Ich habe mir das Paradies immer als eine Art Bibliothek vorgestellt"
id = "edition_berlin"

lieu = [] 
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  =  [
		"HOHMANN, Katharina, TEN HOVEL, Christiane (dir.), Ich habe mir dea Paradies immer als eine Art Bibliothek vorgestellt, Berlin, Revolver publishing, 2016", 
		"édition, 8,5 x 12 cm"
		]
	description = "Recueil de projets imaginaires opérants sur le rapprochement potentiel entre paradis et bibliothèque, énoncé par Jorge Luis Borges (“Siempre imaginé que el Paraíso sería algún tipo de biblioteca”). Fictions proposées par Bettina Allamoda, Arnold Dreyblatt, Eckhard Etzold, Nina Fischer / Maroan El Sani, Ceel Mogami de Haas, Adib Fricke, Aloïs Godinat, Kurt Grunow / Harry Walter, Katahrina Hohmann, Christiane ten Hoevel, Kristen Johannsen, Marine Kaiser, Stefan Krüstemper, Juliane Laitzsch, Seraphina Lenz, Isa Melsheimer / Annette Kisling, Michaela Nasoetion, Inken Reinert, Eva-Maria Schön, Ambroise Tièche, Stella Veciana et Ella Ziegler pour l’Amerika Gedenkbibliothek de Berlin."
+++
