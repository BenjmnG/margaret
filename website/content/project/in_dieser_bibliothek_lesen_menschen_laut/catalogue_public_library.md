+++
draft = false
date = "2016-06-01"

type = "projet"
layout= "un_carrousel"
map = false

title = "Public Library"
id = "catalogue_public_library"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  =  [
		"HOHMANN, Katharina, TEN HOVEL, Christiane (dir.), Public Library, Berlin, Revolver publishing, 2016",
		"édition, 21 x 29,7 cm"
		]
	description = "Catalogue de l'exposition Public Library curatée par Katharina Hohmann et Chistiane ten Hoevel ayant  pris place du 08.09.2016 au 13.10.2016 dans l'Amerika-Gedenkbibliothek de Berlin. <br> Interventions de Bettina Allamoda, Arnold Dreyblatt, Eckhard Etzold, Nina Fischer / Maroan El Sani, Adib Fricke, Kurt Grunow / Harry Walter, Katahrina Hohmann, Christiane ten Hoevel, Kristen Johannsen, Marine Kaiser, Stefan Krüstemper, Juliane Laitzsch, Seraphina Lenz, Michaela Nasoetion, Inken Reinert, Eva-Maria Schön, Roman Signer, Ambroise Tièche, Stella Veciana, Ella Ziegler."

+++


