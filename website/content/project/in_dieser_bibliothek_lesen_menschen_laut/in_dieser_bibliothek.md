+++
draft = false
date = "2016-06-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "In dieser Bibliothek lesen Menschen laut"
id = "in_dieser_bibliothek"

lieu = [ "AGB, Berlin" ]
collaboration_avec = []
commanditaire =  [ "" ]
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  =  [
		"In dieser Bibliothek lesen Menschen laut, 2016", 
		"intervention performative",
		"AGB Berlin"
		]
	description = "Par le micro central, la bibliothécaire propose aux usagers ainsi qu’aux employés de la bibliothèque de lire à voix haute les textes (chiffres, livres, exercices, pages web etc.) qu’ils ont entre les mains. Pour une durée déterminée, on a modifié les règles, perturbé les réponses."
+++
