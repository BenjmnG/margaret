+++
draft = false
date = "2017-06-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "Le Bisou"
id = "le_bisou"

lieu = []
collaboration_avec = [ "Mathias Pfund" ]
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  =  [
		"Le Bisou, 2017",
		"Polystyrène, bois, couverture de chantier, fragment supérieur de la sculpture en pierre l’Abondance de Joseph Witterwulghe (circa 1960)",
		"Friche, Hangars de la Senne, Bruxelles"
		]
+++








