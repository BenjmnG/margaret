+++
draft = false
date = "2017-06-03"

type = "projet"
layout= "un_carrousel" 
map = false

title = "Bisou"
id = "bisou"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  =  [
		"Bisou, 2017",
		"impression laser sur munken 300g/m2, pochette plastique, cordon aluminium, 10 x 6 cm",
		"ODD, Bucarest"
		]
	description = "Étiquette de voyage de l’exposition portative Feed Your Friends. La sculpture Le Bisou photographiée de dos laisse le temps d’apprécier sa cambrure avant de dire Bisou."

+++

