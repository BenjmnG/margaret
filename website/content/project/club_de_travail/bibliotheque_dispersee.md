+++
date = "2017-07-01"
draft = false

type = "projet"
layout= "un_carrousel"
map = false 

title = "Bibliothèque dispersée"
id = "bibliotheque_dispersee"

lieu = [ ]
collaboration_avec = [ "Adeline Molle", "Kathi Seebeck" ]
commanditaire =  []
credit = ""
remerciement = ""
performeurs =  ""
cycle = "Ensemble"

+++

Une bibliothèque à dépôts multiples, l'échange amateur technicisé. Si le travail engage le commun, il se meut aisément en rapports d'autorité. La bibliothèque dispersée constitue un corps élargi de textes réfléchissants le travail, l’emploi, le non-emploi, le temps libre, la pratique de l’art. Constitué de manière collective, les entrées d'ouvrages dispersés dans nos espaces domestiques sont proposées volontairement et un réseau inclusif de pêteurs/emprunteurs s'établit.

Pour consulter le catalogue, emprunter ou proposer une nouvelle entrée, merci d'écrire à clubdetravail@gmail.com.
        