+++
date = "2017-08-01"
draft = false

type = "projet"
layout= "un_carrousel" 
map = false

title = "I would prefer to"
id = "i_would_prefer_to"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs =  ""
cycle = ""

[[legende]]
	image = ["01"]
	contexte  =  [
		"I would prefer to, 2017", 
		"Workshop",
		"Onomatopee, Eindhoven"
		]
	description = "Read underneath my desk, sculpt with left-overs, cook in the water fountain, dance ballet in the elevator and spin my bosses’ swivel chair. Being at work and having to work on a specific task often stimulates the mind to produce stunning phantasies. But how do we use our work time actually? And what possibilities do we have to act differently at work? We discussed our specific practices of perruque – a term Michel de Certeau uses to describe how employees (no matter how restricted they are at work) tend to find a gap within their work situations in order to secretly fulfill their own desires. An afternoon around misappropriation of work time, the redirection of consolidated working policies and the possibility of thereby finding new sense and purpose at work."

+++