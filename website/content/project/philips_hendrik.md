+++
date = "2018-06-01"
draft = false

type = "projet"
layout= "un_carrousel" 
map = true

title = "Philips & Hendrik"
id = "philips_hendrik"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []


[[legende]]
	image = ["01"]
	contexte  = [
		"Philips & Hendrik, 2018",
		"photographie (à partir de l'installation), 240 x 170 cm"
		]
	description = "Le succès de l’entreprise d’ampoules Philips & co amènera à la création d’une ville. En une trentaine d’années, la lumière (électrique) va construire des milliers de logements pour accueillir les employés arrivant en masse. Utilitaires, ces lignes (lijntjes) d’habitats clonés produisent un horizon circulaire non dénué de confort. Au 41, Brugmanstraat, dans Woensel West qui longe Strijp où se trouvent les usines, vit un employé d’usine Hendrik (prénom masculin le plus communément porté au début des années 1920). A peu de chose près, il pourrait avoir l’apparence d’un autre."

+++

