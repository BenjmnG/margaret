+++
draft = true
date = "2014-06-01"

type = "projet"
layout= "un_carrousel" 
map = true

title = "Situations"
id = "situations"

lieu = [ "HEAD, Genève" ]
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
Performeurs = "Performance par Adrian Fernandez Garcia, Marie Matusz, Juliette Luginbuhl, Marianne Sibille, Tania Strautmann, Caroline Tripet"


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Dispersés dans l’espace d’exposition, six performeurs lisent à voix haute.
Une femme lit les sous-titres d’Un Cas pour un bourreau débutant de Pavel Jurácek, une femme entame chacunes des lignes du Don Quichotte de Miguel de Cervantes sans avoir le temps de les terminer, une femme déclame les noms communs du dictionnaire sans leurs définitions, une femme chante de manière lyrique un nom d’artiste par respiration, une femme reproduit l’air de l’action de feuilleter les livres de la bibliothèque, un homme donne vite le sommaire d'Une Brève histoire du monde de Gombrich. Les spectateurs circulent à travers leurs présences et leurs voix.