+++
draft = false
date = "2017-05-01"

type = "projet"
layout= "un_carrousel" 
map = true

title = "Suzanne"
id = "suzanne"

lieu = [ "Bibliothèque des sciences humaines de l’Université libre de Bruxelles." ]
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

+++

Dédicace de Suzanne Weenink, partenaire de vie et de travail d’Erik van Lieshout lors de la sortie du catalogue de l’exposition d’Erik van Lieshout sur la première page du livre emprunté *Artistes sans oeuvres* de Jean-Yves Jouannais. Livre aujourd’hui rendu à la bibliothèque des sciences humaines de l’Université libre de Bruxelles.

