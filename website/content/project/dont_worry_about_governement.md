+++
draft = false
date = "2018-11-30"

type = "projet"
layout= "un_carrousel" 
map = true

title = "Don't Worry about the Government"
subtitle = ["Intérieur d’accompagnement", "Snack typique", "Fluid life"]
id = "dont_worry_about_governement"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = "Photographies par Romain Duhamel et Ioannis Pallas"
remerciement = "Thanks to Mathias Pfund, Giulia Blasig, Dounia Mojahid, Romain Duhamel, Marianne & Alex den Ouden, Jessie Derogy, Anastasia Eggers, Steph Meul, Marek Tomaszewski, Zakaria Assimi, Artist Commons, Brico Barrière et le Golden Night Shop"
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["03"]
	contexte  = [
		"Intérieur d'accompagnement, 2019", 
		"Installation in situ, peinture terra cotta, canalisations pvc, flexible, colsons, colliers, céramique émaillée, dimensions variables", 
		"Cunst-link, rue Théodore Verhaegen 154, Bruxelles"
		]
	description = "En 1623, Pierre Le Muet publie Manière de bâtir pour toutes sortes de personnes à la demande du roi de de France et de Navarre Henri IV. Pour assoir son pouvoir, ce dernier va prendre en charge l’espace public en décidant d’unifier son esthétique. Dès lors, l’architecture ne sera plus tributaire de la fonction de son bâtiment mais répondra aux normes de la monarchie. Pierre le Muet dresse alors un catalogue de style aussi pratique que directeur, qui prendra le nom «d’architecture d’accompagnement». En opérant un déplacement par élongation, Intérieur d’accompagnement structure Cunst-Link en bricolant une canalisation qui transporte l’eau de l’extérieur vers un trou appartenant à l’espace d’exposition. Un nouvel intermédiaire, qui se joue collectif, dans le système de distribution de l’eau."


[[legende]]
	image = ["10"]
	contexte  =  [
		"Snack typique, 2019", 
		"Intervention orale / tampon, samoussas, essuies-tout, 23 x 24,5 cm", 
		"Golden NightShop, rue Théodore Verhaegen 117, Bruxelles"
		]
	description = "La communauté pakistanaise gère essentiellement les night-shops bruxellois. Alors que ceux-ci ne proposent que des produits industriels et globalisés (soda, snacks, cigarettes etc), quelques night-shops vendent (bien que celle-ci soit interdite) également des samoussas fraîchement cuisinés qui convoquent une localité. Snack typique est le code donné oralement aux visiteurs de l’exposition (exposition composée d’Intérieur d’accompagnement, Snack typique et Fluid Life ayant lieu dans trois espaces d’une même rue). Ceux-ci sont ainsi invités à demander quelque chose d’incertain au patron du night shop. En échange de ce code, le patron leur offre un samoussa gracieusement (dans un essuie-tout tamponné, certifiant la transaction). En inversant les rapports de pouvoir entre commerçant et client, Snack typique développe les fonctionnements et fondements de l’informel, un savoir et une confiance partagés."


[[legende]]
	image = ["13"]
	contexte  = [
		"Fluid Life, 2018", 
		"édition de 31 dessins plastifiés, 33,3 x 21,6 cmn", 
		"Brico Barrière, rue Théodore Verhaegen 64, Bruxelles"
		]
	description = "Une série de grilles dont la structure se confond avec ce qui passe à l’intérieur est plastifiée pour s’adapter à son milieu, un magasin de bricolage (qui porte le nom de la grande chaîne «Brico» sans y appartenir). Ce dernier incarnant selon Michel de Certeau la propension individuelle à faire les choses à sa manière, à travers ou à côté des normes et constructions. Les expériences du white cube et de celles du magasin de bricolage se mêlent, tout comme ses publics et rencontres."

+++

Questioning the link between formal and informal practices, Don’t Worry about the Government is spread across three spaces along the street of Théodore Verhaegen in Saint-Gilles: at Cunst-link where the site-specific installation Intérieur d’accompagnement is presented, on the other side of the street, Snack Typique happens at the Golden Night Shop and in the shelves of Brico Barrière, the editions of drawings Fluid Life is presented. All these spaces embody a reflection on intimate and public circulation of commodities, power and representations. This spread exhibition does not just create a circulation of people along the street Théodore Verhaegen, but it is also an occasion for different communities and cultures that live in the same area but who do not necessary intercommunicate, to get acquainted and cooperate together.
<br><br>
[Press Release](https://artistcommons.net/wp-content/uploads/2019/02/pressrelease_marinekaiser_EN-1.pdf?fbclid=IwAR1uk1wAkPgCLO73mr6E3iTzfPMNzAseeaSGJw0JRl_TrYEUF2ESGAbqZAo) / [Communiqué de presse](https://artistcommons.net/wp-content/uploads/2019/02/communiquedepresse_marinekaiser_FR-1.pdf?fbclid=IwAR3eUke5d3EZlOgpBo1WAMtPlJN3_Mc2iqIXm04nKWIDh6AJVao-wft2hbo)