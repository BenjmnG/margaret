+++
date = "2018-07-01"
draft = false

type = "projet"
layout= "un_carrousel" 
map = true

title = "Lucky"
id = "lucky"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  = [
		"Lucky, 2018",
		"photographie (à partir de la sculpture du banc), 240 x 180 cm"
	]
	description = "Comme de nombreux acteurs de la vie collective, le salon de coiffure Al Yas est tenu par un propriétaire immigré. Inspiré de la scénographie de Sergio Gerstein pour la première représentation de la pièce En Attendant Godot au théâtre de Babylone, le très tendre banc Lucky accompagne l’attente d’une coupe de cheveux ou évoque celle des décisions administratives."

+++