+++
date = "2017-06-01"
draft = false

type = "projet"
layout= "un_carrousel" 
map = true

title = "LAT (Living Apart Together)"
id = "lat"

lieu = [ "Centre d'art contemporain, vernissage des bourses de la ville, Genève" ]
collaboration_avec = [ "Mathias Pfund" ]
commanditaire =  []
credit = "Photographie par Raphaëlle Mueller"
remerciement = ""
performeurs =  ""

[[legende]]
	image = ["01"]
	contexte  =  [
		"LAT (Living Apart Together), 2017",
		"bois, serre-joints, couverture de chantier, 160 x 140 x 40 cm",
		"centre d'art contemporain, Genève"
		]
	description = "Vivre ensemble ou tenir ensemble, séparément. Pour l'autonomie, chaque élément est inscrit en gravure. Les serre-joints accordent une organisation et ce soir, on en fait un bar pour le vernissage."

+++