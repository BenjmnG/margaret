+++
draft = false
date = "2018-05-01"

type = "projet"
layout= "un_carrousel" 
map = true

title = "Leste"
id = "leste"

lieu = []
collaboration_avec = [ "Filippo Minelli" ]
commanditaire =  []
credit = ""
remerciement = "Thanks to Freek Lomme, Benjamin Gremillon, Mathias Pfund"
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  = [
		"Leste, 2018", 
		"Flag, 150 x 100 cm", 
		"In the framework of Across the Border, a collaborative project initiated by Filippo Minelli.",
		"Palazzo Ajutamicristo, Manifesta 12, Palermo"
		]
	description = "Leste blows in the South if I say it from my perspective. <br> Getting through the velux to enjoy a barbecue fed with the fan bought to cool off the attic room becomes a surrogate of what the family calls home."
+++
