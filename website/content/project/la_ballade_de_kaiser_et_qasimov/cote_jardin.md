+++
draft = false
date = "2017-06-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "Côté jardin"
id = "cote_jardin"

lieu = []
collaboration_avec = ["Stéphanie Verin"]
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  =  [
		"Côté jardin, 2017", 
		"Vidéo, 6.45min", 
		"Images: Ilaria Fantini",
		"Sons diffusés par le lampadaire: Alex Adair,  Philip George",
		"Texte: Michel Baridon"
		]
	description = "Projection de l’espace public sur le parvis du centre commercial."

+++