+++
draft = true
date = "2017-06-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "La Roue - Het Rad"
id = "la_roue"

lieu = []
collaboration_avec = ["Stéphanie Verin"]
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Dessin préparatoire à la production du banc gonflable à 36 assises La Roue-Het Rad. Inspiré par l'ouvrage Utopia de Bernadette Mayer ainsi que par nos balades à travers de multiples formes de collectivité, ce banc se gonflera en dispositif scénographique de la future performance Green is mine.

